cp -r --parents ./target/clover HadoopCloverReport
cp -r --parents ./target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-project/target/clover HadoopCloverReport
cp -r --parents ./hadoop-project/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-httpfs/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-httpfs/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs/src/contrib/bkjournal/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs/src/contrib/bkjournal/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-rbf/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-rbf/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-native-client/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-native-client/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-nfs/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-nfs/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-client/target/clover HadoopCloverReport
cp -r --parents ./hadoop-hdfs-project/hadoop-hdfs-client/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-client/target/clover HadoopCloverReport
cp -r --parents ./hadoop-client/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-maven-plugins/target/clover HadoopCloverReport
cp -r --parents ./hadoop-maven-plugins/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-registry/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-registry/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-ui/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-ui/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-client/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-client/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-applications/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-applications/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-applications/hadoop-yarn-applications-unmanaged-am-launcher/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-applications/hadoop-yarn-applications-unmanaged-am-launcher/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-applications/hadoop-yarn-applications-distributedshell/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-applications/hadoop-yarn-applications-distributedshell/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-site/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-site/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-web-proxy/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-web-proxy/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-sharedcachemanager/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-sharedcachemanager/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-common/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-common/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-tests/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-tests/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-applicationhistoryservice/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-applicationhistoryservice/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-resourcemanager/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-resourcemanager/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timelineservice/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timelineservice/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timeline-pluginstorage/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timeline-pluginstorage/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-nodemanager/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-nodemanager/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timelineservice-hbase-tests/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timelineservice-hbase-tests/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-router/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-router/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timelineservice-hbase/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-timelineservice-hbase/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-common/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-common/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-api/target/clover HadoopCloverReport
cp -r --parents ./hadoop-yarn-project/hadoop-yarn/hadoop-yarn-api/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-common/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-common/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-core/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-core/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-hs-plugins/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-hs-plugins/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-shuffle/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-shuffle/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-jobclient/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-jobclient/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-hs/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-hs/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-app/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-client/hadoop-mapreduce-client-app/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-examples/target/clover HadoopCloverReport
cp -r --parents ./hadoop-mapreduce-project/hadoop-mapreduce-examples/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-project-dist/target/clover HadoopCloverReport
cp -r --parents ./hadoop-project-dist/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-build-tools/target/clover HadoopCloverReport
cp -r --parents ./hadoop-build-tools/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-assemblies/target/clover HadoopCloverReport
cp -r --parents ./hadoop-assemblies/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-cloud-storage-project/target/clover HadoopCloverReport
cp -r --parents ./hadoop-cloud-storage-project/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-cloud-storage-project/hadoop-cloud-storage/target/clover HadoopCloverReport
cp -r --parents ./hadoop-cloud-storage-project/hadoop-cloud-storage/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-dist/target/clover HadoopCloverReport
cp -r --parents ./hadoop-dist/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-minicluster/target/clover HadoopCloverReport
cp -r --parents ./hadoop-minicluster/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-aliyun/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-aliyun/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-rumen/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-rumen/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-archive-logs/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-archive-logs/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-sls/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-sls/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-extras/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-extras/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-openstack/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-openstack/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-archives/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-archives/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-datajoin/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-datajoin/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-gridmix/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-gridmix/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-azure-datalake/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-azure-datalake/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-tools-dist/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-tools-dist/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-pipes/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-pipes/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-azure/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-azure/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-resourceestimator/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-resourceestimator/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-aws/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-aws/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-streaming/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-streaming/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-ant/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-ant/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-distcp/target/clover HadoopCloverReport
cp -r --parents ./hadoop-tools/hadoop-distcp/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-kms/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-kms/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-auth-examples/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-auth-examples/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-auth/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-auth/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-nfs/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-nfs/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-minikdc/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-minikdc/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-annotations/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-annotations/target/clover/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-common/target/clover HadoopCloverReport
cp -r --parents ./hadoop-common-project/hadoop-common/target/clover/clover HadoopCloverReport
